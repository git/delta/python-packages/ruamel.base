# coding: utf-8

from __future__ import print_function

import pytest          # QAOK


# default for tox stub is to Fail
def test_base():
    import ruamel
    with pytest.raises(ImportError):
        import ruamel.std
        print(ruamel.std.__file__)   # to suppress flake msg
    assert True
